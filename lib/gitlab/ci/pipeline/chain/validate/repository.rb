# frozen_string_literal: true

module Gitlab
  module Ci
    module Pipeline
      module Chain
        module Validate
          class Repository < Chain::Base
            include Chain::Helpers

            def perform!
<<<<<<< HEAD
              unless @command.branch_exists? || @command.tag_exists? || @command.merge_request_ref_exists?
=======
              unless @command.branch_exists? || @command.tag_exists?
>>>>>>> test-v10.3.1
                return error('Reference not found')
              end

              unless @command.sha
                return error('Commit not found')
              end

              if @command.ambiguous_ref?
                return error('Ref is ambiguous')
              end
            end

            def break?
              @pipeline.errors.any?
            end
          end
        end
      end
    end
  end
end
