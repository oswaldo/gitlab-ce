# frozen_string_literal: true

module Gitlab
  module Ci
    module Pipeline
      module Chain
        class Build < Chain::Base
          def perform!
            @pipeline.assign_attributes(
              source: @command.source,
              project: @command.project,
              ref: @command.ref,
              sha: @command.sha,
              before_sha: @command.before_sha,
<<<<<<< HEAD
              source_sha: @command.source_sha,
              target_sha: @command.target_sha,
=======
>>>>>>> test-v10.3.1
              tag: @command.tag_exists?,
              trigger_requests: Array(@command.trigger_request),
              user: @command.current_user,
              pipeline_schedule: @command.schedule,
<<<<<<< HEAD
              merge_request: @command.merge_request,
              variables_attributes: Array(@command.variables_attributes)
=======
              protected: @command.protected_ref?
>>>>>>> test-v10.3.1
            )

            @pipeline.set_config_source
          end

          def break?
            false
          end
        end
      end
    end
  end
end
