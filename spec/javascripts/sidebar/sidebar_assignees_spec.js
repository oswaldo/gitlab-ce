import _ from 'underscore';
import Vue from 'vue';
import SidebarAssignees from '~/sidebar/components/assignees/sidebar_assignees.vue';
import SidebarMediator from '~/sidebar/sidebar_mediator';
import SidebarService from '~/sidebar/services/sidebar_service';
import SidebarStore from '~/sidebar/stores/sidebar_store';
import mountComponent from 'spec/helpers/vue_mount_component_helper';
import Mock from './mock_data';
import mountComponent from '../helpers/vue_mount_component_helper';

describe('sidebar assignees', () => {
  let vm;
  let mediator;
  let sidebarAssigneesEl;
<<<<<<< HEAD
  preloadFixtures('issues/open-issue.html');
=======
  preloadFixtures('issues/open-issue.html.raw');
>>>>>>> test-v10.3.1

  beforeEach(() => {
    Vue.http.interceptors.push(Mock.sidebarMockInterceptor);

<<<<<<< HEAD
    loadFixtures('issues/open-issue.html');
=======
    loadFixtures('issues/open-issue.html.raw');
>>>>>>> test-v10.3.1

    mediator = new SidebarMediator(Mock.mediator);
    spyOn(mediator, 'saveAssignees').and.callThrough();
    spyOn(mediator, 'assignYourself').and.callThrough();

    const SidebarAssigneeComponent = Vue.extend(SidebarAssignees);
    sidebarAssigneesEl = document.querySelector('#js-vue-sidebar-assignees');
<<<<<<< HEAD
    vm = mountComponent(
      SidebarAssigneeComponent,
      {
        mediator,
        field: sidebarAssigneesEl.dataset.field,
      },
      sidebarAssigneesEl,
    );
=======
    vm = mountComponent(SidebarAssigneeComponent, {
      mediator,
      field: sidebarAssigneesEl.dataset.field,
    }, sidebarAssigneesEl);
>>>>>>> test-v10.3.1
  });

  afterEach(() => {
    SidebarService.singleton = null;
    SidebarStore.singleton = null;
    SidebarMediator.singleton = null;
    Vue.http.interceptors = _.without(Vue.http.interceptors, Mock.sidebarMockInterceptor);
  });

  it('calls the mediator when saves the assignees', () => {
    vm.saveAssignees();
<<<<<<< HEAD

=======
>>>>>>> test-v10.3.1
    expect(mediator.saveAssignees).toHaveBeenCalled();
  });

  it('calls the mediator when "assignSelf" method is called', () => {
    vm.assignSelf();

    expect(mediator.assignYourself).toHaveBeenCalled();
    expect(mediator.store.assignees.length).toEqual(1);
  });

<<<<<<< HEAD
  it('hides assignees until fetched', done => {
    const currentAssignee = sidebarAssigneesEl.querySelector('.value');

=======
  it('hides assignees until fetched', (done) => {
    const currentAssignee = sidebarAssigneesEl.querySelector('.value');
>>>>>>> test-v10.3.1
    expect(currentAssignee).toBe(null);

    vm.store.isFetching.assignees = false;
    Vue.nextTick(() => {
      expect(vm.$el.querySelector('.value')).toBeVisible();
      done();
    });
  });
});
