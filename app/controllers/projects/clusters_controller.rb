# frozen_string_literal: true

class Projects::ClustersController < Clusters::ClustersController
  prepend_before_action :project
  before_action :repository

  before_action do
    push_frontend_feature_flag(:prometheus_computed_alerts)
  end

  layout 'project'

  private

<<<<<<< HEAD
  def clusterable
    @clusterable ||= ClusterablePresenter.fabricate(project, current_user: current_user)
=======
  def cluster
    @cluster ||= project.clusters.find(params[:id])
                                 .present(current_user: current_user)
  end

  def create_params
    params.require(:cluster).permit(
      :enabled,
      :name,
      :provider_type,
      provider_gcp_attributes: [
        :gcp_project_id,
        :zone,
        :num_nodes,
        :machine_type
      ])
  end

  def update_params
    if cluster.managed?
      params.require(:cluster).permit(
        :enabled,
        :environment_scope,
        platform_kubernetes_attributes: [
          :namespace
        ]
      )
    else
      params.require(:cluster).permit(
        :enabled,
        :name,
        :environment_scope,
        platform_kubernetes_attributes: [
          :api_url,
          :token,
          :ca_cert,
          :namespace
        ]
      )
    end
>>>>>>> test-v10.3.1
  end

  def project
    @project ||= find_routable!(Project, File.join(params[:namespace_id], params[:project_id]))
  end

  def repository
    @repository ||= project.repository
  end
end
