import axios from '../../lib/utils/axios_utils';

export default class MRWidgetService {
  constructor(endpoints) {
<<<<<<< HEAD
    this.endpoints = endpoints;
=======
    this.mergeResource = Vue.resource(endpoints.mergePath);
    this.mergeCheckResource = Vue.resource(`${endpoints.statusPath}?serializer=widget`);
    this.cancelAutoMergeResource = Vue.resource(endpoints.cancelAutoMergePath);
    this.removeWIPResource = Vue.resource(endpoints.removeWIPPath);
    this.removeSourceBranchResource = Vue.resource(endpoints.sourceBranchPath);
    this.deploymentsResource = Vue.resource(endpoints.ciEnvironmentsStatusPath);
    this.pollResource = Vue.resource(`${endpoints.statusPath}?serializer=basic`);
    this.mergeActionsContentResource = Vue.resource(endpoints.mergeActionsContentPath);
>>>>>>> test-v10.3.1
  }

  merge(data) {
    return axios.post(this.endpoints.mergePath, data);
  }

  cancelAutomaticMerge() {
    return axios.post(this.endpoints.cancelAutoMergePath);
  }

  removeWIP() {
    return axios.post(this.endpoints.removeWIPPath);
  }

  removeSourceBranch() {
    return axios.delete(this.endpoints.sourceBranchPath);
  }

  fetchDeployments(targetParam) {
    return axios.get(this.endpoints.ciEnvironmentsStatusPath, {
      params: {
        environment_target: targetParam,
      },
    });
  }

  poll() {
    return axios.get(`${this.endpoints.statusPath}?serializer=basic`);
  }

  checkStatus() {
    return axios.get(`${this.endpoints.statusPath}?serializer=widget`);
  }

  fetchMergeActionsContent() {
    return axios.get(this.endpoints.mergeActionsContentPath);
  }

  rebase() {
    return axios.post(this.endpoints.rebasePath);
  }

  static stopEnvironment(url) {
    return axios.post(url);
  }

  static fetchMetrics(metricsUrl) {
    return axios.get(`${metricsUrl}.json`);
  }
}
