  ## GitLab core team & GitLab Inc. contribution process

  ---

  <!-- START doctoc generated TOC please keep comment here to allow auto update -->
  <!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
  **Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

  - [Purpose of describing the contributing process](#purpose-of-describing-the-contributing-process)
  - [Common actions](#common-actions)
    - [Merge request coaching](#merge-request-coaching)
  - [Assigning issues](#assigning-issues)
  - [Be kind](#be-kind)
  - [Bugs](#bugs)
    - [Regressions](#regressions)
    - [Managing bugs](#managing-bugs)
  - [Release retrospective and kickoff](#release-retrospective-and-kickoff)
  - [Copy & paste responses](#copy--paste-responses)
    - [Improperly formatted issue](#improperly-formatted-issue)
    - [Issue report for old version](#issue-report-for-old-version)
    - [Support requests and configuration questions](#support-requests-and-configuration-questions)
    - [Code format](#code-format)
    - [Issue fixed in newer version](#issue-fixed-in-newer-version)
    - [Improperly formatted merge request](#improperly-formatted-merge-request)
    - [Accepting merge requests](#accepting-merge-requests)
    - [Only accepting merge requests with green tests](#only-accepting-merge-requests-with-green-tests)

  <!-- END doctoc generated TOC please keep comment here to allow auto update -->
